<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit = no" >
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="/test/armada-web-project/CSS/style.css">
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
      <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>     
      <title>Page de Edition Bateaux</title>
  </head>

  <!--Barre de navigation-->
  <?php
        $state1 = "inactive"; 
        $state2 = "inactive"; 
        $state3 = "inactive"; 
        $state4 = "active"; 
        $state5 = "inactive"; 
        $respo = "";
        $admin = "";
         
        if (empty($_SESSION['nom'])) {
            $links = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Connexion.php">Se Connecter</a>';
        } else {
            $links = '<a class="nav-link text-danger" href="/test/armada-web-project/HTML_PHP/Deconnexion.php">Deconnexion</a>';
        }
        if(isset($_SESSION['niv'])){
            switch($_SESSION['niv']){
                case 2:
                    $respo = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Editer_Bateau.php">Editer Bateau</a>';
                break;
                case 3:
                    $admin = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Admin.php">Consulter droits acces</a>';
                break;
            }
        }
  ?>

  <?php include("header.php");?>

  
  <body>
        <!--Texte d'acceuil-->
        <?php 
          $active_c = "active"; $show_active_c = "show active"; $aria_c = "true";
          $active_m = $show_active_m = $aria_m = "";
          $active_d = $show_active_d = $aria_d = "";
        
        if(isset($_GET['tab'])){

            if($_GET['tab']=="m"){
                $active_c = ""; $show_active_c = ""; $aria_c = "";
                $active_m = "active"; $show_active_m = "show active"; $aria_m = "true";
                $active_d = ""; $show_active_d = ""; $aria_d = "";
            }

            if($_GET['tab']=="d"){
                $active_c = ""; $show_active_c = ""; $aria_c = "";
                $active_m = ""; $show_active_m = ""; $aria_m = "";
                $active_d = "active"; $show_active_d = "show active"; $aria_d = "true";
            }
        }
        
        ?>

        <div id = "conn" class="container">
            <div class="card-header">
                <nav>
                    <div class="nav nav-tabs card-header-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link <?php echo $active_c;?>" id="nav-home-tab" data-toggle="tab" href="#creer" role="tab" aria-controls="creer" aria-selected="<?php echo $aria_c;?>">Creer</a>
                        <a class="nav-item nav-link <?php echo $active_m;?>" id="mod-tab" data-toggle="tab" href="#mod" role="tab" aria-controls="mod" aria-selected="<?php echo $aria_m;?>">Modifier</a>
                        <a class="nav-item nav-link <?php echo $active_d;?>" id="del-tab" data-toggle="tab" href="#del" role="tab" aria-controls="del" aria-selected="<?php echo $aria_d;?>">Supprimer</a>
                    </div>
                </nav>
            </div>

            <?php $id_respo = $_SESSION['id']; 
                include("traitement_bateau.php"); 
                include("traitement_bat_mod.php");
            
                if($_SESSION['niv']=="")
                {
                    echo '<div style = "margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">';
                    echo '<strong>Veuillez vous reconnecter s\'il vous plait <i class="em em-anchor"></i></strong>';
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                    echo '<span aria-hidden="true">&times;</span>';
                    echo '</button>';
                    echo '</div>';

                }
                else{
                    include("Gestion_bateau.php");
                }             
            ?>

        </div>
  </body>

<?php include("footer.inc.php"); ?>