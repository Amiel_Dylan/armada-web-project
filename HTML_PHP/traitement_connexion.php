<?php
    $password = $identifiant = $box = $errPass = $errId = "";
    $succes = true;
    $servername = "localhost";
    $username = "root";
    $mdp = "";
    $myDataBaseName = "armada_projet";

    if(isset($_POST["submit"])) 
    {
         // Verifie si le nom est bien entré
        if (empty($_POST["identifiant"])) {
            $errId = "* Veuillez indiquer vos identifiants!";
            $succes = false;
        } else {
            $identifiant = test_input($_POST["identifiant"]);
            // Verifie si les caracteres sont correctes
            if (!preg_match("/^[a-zA-Z ]*$/",$identifiant)) {
              $errId = "* Seul les lettres et l'espace sont autorisés"; 
              $succes = false;
            }
        }

        if (empty($_POST["password"])) {
            $errPass = "* Veuillez indiquer le mot de passe!";
            $succes = false;
        } else {
            $password = test_input($_POST["password"]);
            // Verifie si les caracteres sont correctes
            if (strlen($password) < 8) {
              $errPass = "* Mot de passe trop court !";
            }
            if (!(preg_match('#^(?=.*[a-z])#', $password))) {
              $errPass = "* Mot de passe incorrecte"; 
              $succes = false;
            }
        }
        
        if($succes) 
        {

            if (!empty($_POST["checkbox"])) {
                setcookie("identifiant",$identifiant,time()+(86400*30));
                setcookie("password",$password,time()+(86400*30));
            }else{
                setcookie("identifiant","",time()-1000);
                setcookie("password","",time()-1000);
            }

            $conn = mysqli_connect($servername, $username, $mdp, $myDataBaseName); // Create connection
            // Check connection
            if (!$conn){
               die("Connection failed: " . mysqli_connect_error());
            }

            $pwd_hash = md5($password);
            $sql = mysqli_query($conn,"SELECT * FROM utilisateur WHERE identifiant = '$identifiant' AND password = '$pwd_hash'");
            $count = mysqli_num_rows($sql);
            $tableau = mysqli_fetch_assoc($sql);
            $id = $tableau['id_user'];
            $_SESSION['niv'] = $tableau['niveau'];
            $_SESSION['nom'] = $tableau['nom'];
            $_SESSION['prenom'] = $tableau['prenom'];
            $_SESSION['id'] = $tableau['id_user'];

                if($count == 1){
                    header("Status: 301 Moved Permanently", false, 301);
                    header("Location:/test/armada-web-project/HTML_PHP/index.php");
                    exit();
                }
                else{
                    $_SESSION['mes'] = "ncon";
                    header("Status: 301 Moved Permanently", false, 301);
                    header('Location:/test/armada-web-project/HTML_PHP/Connexion.php');
                    exit(); 
                }
            

            if (!mysqli_query($conn, $sql)) {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
            
        }   
    }
 
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }
?> 