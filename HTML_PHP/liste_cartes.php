<div class="card-deck">
        <?php 
            while($bateau= mysqli_fetch_array($bat)){
                $i++;
                $ide = $bateau["id_bat"];
                $nom = $bateau["nom"];
                $pays = $bateau["pays"];
                $taille = $bateau["taille"];
                $image = $bateau["image"];

                echo'<div class="card mb-4 border border-secondary">';
                echo'   <img class="card-img-top" src="'.$dossier_img.$image.'" alt="Card image cap">';
                echo'       <div class="card-body">';
                echo'           <h5 class="card-title">'.$nom.'</h5>';
                echo'           <p class="card-text"><hr>
                                    <strong>Pays de provenance : </strong>'.$pays.'<br>
                                    <strong>Taille : </strong>'.$taille.' m<br>
                                </p>';
                echo'       </div>';
                echo'</div>';

                // wrap every 2 on sm
                if($i==2 || $i == 4 || $i == 6 || $i == 8){
                    echo '<div class="w-100 d-none d-sm-block d-md-none">';
                    echo '</div>';
                }

                if($i==2 || $i == 6 || $i == 9){
                    echo '<div class="w-100 d-none d-md-block d-lg-none">';
                    echo '</div>';
                }

                if($i==4 || $i == 8){
                    echo '<div class="w-100 d-none d-lg-block d-xl-none">';
                    echo '</div>';
                }

                // wrap every 5 on xl
                if($i==5){
                    echo '<div class="w-100 d-none d-xl-block">';
                    echo '</div>';
                }

            }                                       
        ?>
  </div>