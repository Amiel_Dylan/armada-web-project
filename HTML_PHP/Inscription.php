<?php session_start()?>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit = no" >
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="http://localhost/test/armada-web-project/CSS/style.css">
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <title>Page Inscription</title>
  </head>
  
  <?php
  $state1 = "inactive";
  $state2 = "inactive";
  $state3 = "active";
  $state4 = "inactive";
  $state5 = "inactive";
  $respo = "";
  $admin = "";

  if (empty($_SESSION['nom'])) {
    $links = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Connexion.php">Se Connecter</a>';
  } else {
    $links = '<a class="nav-link text-danger" href="/test/armada-web-project/HTML_PHP/Deconnexion.php">Deconnexion</a>';
  }
  if (isset($_SESSION['niv'])) {
    switch ($_SESSION['niv']) {
      case 2:
        $respo = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Editer_Bateau.php">Editer Bateau</a>';
        break;
      case 3:
        $admin = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Admin.php">Consulter droits acces</a>';
        break;
    }
  }
  ?>

  <?php include("header.php"); ?>

  <body>
      <!-- Corps de la page-->
    <?php include("traitement_inscription.php"); ?>
    <div id="ins" class= "container">
      <div>
      <?php if(isset($_SESSION['mes'])){
        if($_SESSION['mes'] == "dbl"){
            echo '<div class="alert alert-info alert-dismissible fade show" role="alert">';
            echo '<strong>Cet identifiant et ce mot de passe sont déjà attribué à un autre compte! Veuillez à nouveau remplir le formulair ou vous <a href="/test/armada-web-project/HTML_PHP/Connexion.php">Connecter</a>.'; 
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';
         }}
      ?>
      
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div>
          <h1 class="text-center">Vos coordonnees personnelles</h1><hr>
            <div class="form-row d-flex justify-content-center">
                <div class="form-group col-md-5">
                  <label for="nom">Nom</label>
                  <input type="text" name ="nom" class="form-control" id="nom" value="<?php echo $nom; ?>">
                  <span  class="text-danger"><?php echo $errNom; ?></span>
                </div>
                <div class="form-group col-md-5">
                  <label for="prenom">Prénom</label>
                  <input type="text" name ="prenom" class="form-control" id="prenom" value="<?php echo $prenom; ?>">
                  <span  class="text-danger"><?php echo $errPrenom; ?></span>
                </div>
                <div class="form-group col-md-10">
                  <label for="mail">E-mail</label>
                  <input type="email" name="email" id="mail" class="form-control" value="<?php echo $email; ?>">
                  <span  class="text-danger"><?php echo $errEmail; ?></span>
                  <small id="passwordHelpBlock" class="form-text text-muted">Votre e-mail sera utilisé pour renvoyer le mot de passe en cas de perte</small>
                </div>
            </div>  
            
          <h1 class="text-center">Vos Identifiants</h1><hr>
            <div class="form-row d-flex justify-content-center">
                <div class="form-group col-md-5">
                  <label for="id">Identifiant</label>
                  <input type="text" name ="id" class="form-control" id="id" value="<?php echo $id; ?>">
                  <span  class="text-danger"><?php echo $errId; ?></span>
                </div>
                <div class="form-group col-md-5">
                  <label for="id_conf">Confirmer Identifiant</label>
                  <input type="text" name ="id_conf" class="form-control" id="id_conf" value="<?php echo $id_conf; ?>">
                  <span  class="text-danger"><?php echo $errId_conf; ?></span>
                </div>
                <div class="form-group col-md-5">
                  <label for="pwd">Mot de passe</label>
                  <input type="password" name="password" id="pwd" class="form-control" pattern=".{8,}" required title="8 caracteres minimum" value="<?php echo $password; ?>">
                  <span  class="text-danger"><?php echo $errPassword; ?></span>
                  <small id="passwordHelpBlock" class="form-text text-muted">Indiquez un mot de passe à 8 characteres au moins</small>
                </div>
                <div class="form-group col-md-5">
                  <label for="pwd_conf">Confirmer Mot de Passe</label>
                  <input type="password" name="password_conf" id="pdw_conf" class="form-control" value="<?php echo $password_conf; ?>">
                  <span  class="text-danger"><?php echo $errPassword_conf; ?></span>
                  <small id="passwordHelpBlock" class="form-text text-muted">Indiquez un mot de passe à 8 characteres au moins</small>
                </div>
            </div>  

            <div class="form-group d-flex justify-content-around">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="gridCheck">
                  <label class="form-check-label" for="gridCheck">Se souvenir de moi</label>
                </div>
            </div>

            <p class="text-center"> <button type="submit" name = "submit" class="btn btn-primary">  S'inscrire  </button> </p>
          </div>
        </form>
      </div> 
    </div>          
  </body>

  <?php include("footer.inc.php"); ?>
</html>