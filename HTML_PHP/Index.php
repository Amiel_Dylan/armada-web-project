<?php session_start()?>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit = no" >
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="/test/armada-web-project/CSS/style.css">
      <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      <link rel="shortcut icon" href="/test/armada-web-project/Images/sailboat.png" />
      <title>Page Acceuil</title>
  </head>

      <?php
        $state1 = "active"; 
        $state2 = "inactive"; 
        $state3 = "inactive"; 
        $state4 = "inactive"; 
        $state5 = "inactive"; 
        $respo = "";
        $admin = "";
         
        if (empty($_SESSION['nom'])) {
            $links = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Connexion.php">Se Connecter</a>';
        } else {
            $links = '<a class="nav-link text-danger" href="/test/armada-web-project/HTML_PHP/Deconnexion.php">Deconnexion</a>';
        }
        if(isset($_SESSION['niv'])){
        switch($_SESSION['niv']){
            case 2:
                $respo = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Editer_Bateau.php">Editer Bateau</a>';
            break;
            case 3:
                $admin = '<a class="nav-link" href="/test/armada-web-project/HTML_PHP/Admin.php">Consulter droits acces</a>';
            break;
        }
    }
  ?>

  <?php include("header.php"); ?>

  <body>
    <!--Texte d'acceuil-->
    <div class="container">

        <?php if(isset($_SESSION['niv'])){
        if($_SESSION['niv']==1){
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
            echo '<strong>Connexion reussi!</strong> Bienvenu '.$_SESSION['nom'].' '.$_SESSION['prenom'].' et bonne visite!';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';
         }
         if($_SESSION['niv']==2){
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
            echo '<strong>Connexion reussi!</strong> Bienvenu <strong> Responsable de bateau</strong> '.$_SESSION['nom'].' '.$_SESSION['prenom'].' et bonne visite!';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';
         }
         if($_SESSION['niv']==3){
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
            echo '<strong>Connexion reussi!</strong> Bienvenu <strong> Administrateur </strong> '.$_SESSION['nom'].' '.$_SESSION['prenom'].' et bonne visite!';
            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            echo '<span aria-hidden="true">&times;</span>';
            echo '</button>';
            echo '</div>';
         }}?>
         
         <div class="row" style="padding : 10px;">

            <div class="col-sm-8">
                 <h2>Bienvenue sur la page d'acceuil!</h2>
                 <p>L'Armada 2019 est organisée par l'association de <a href="http://www.armada.org/armada/l-association">" l'Armada de la liberté "</a>. 
                 Cette manifestation peut une nouvelle fois voir le jour grâce à l'action d'une armada de 
                 bénévoles fédérés autour de leur président, Patrick Herr.</p>
                 <p>Ainsi il fut décidé de faire venir les plus grands voiliers du monde à Rouen pour le bicentenaire de la Révolution. La manifestation fut un tel succès qu'elle devint un rendez-vous régulier. Au fil des éditions, la panoplie de bateaux invités s'ouvrit entre autres aux navires de guerre (porte-hélicoptères, sous-marins…) et aux péniches.
                 C'est une manifestation gratuite pour les visiteurs.</p>
                 <h3>L'arrivée des navires</h3>
                 <p>Les navires arrivent à Rouen au fur et à mesure. Certains ont à démonter en partie le haut de leurs mâts pour pouvoir passer sous les trois ponts en aval de Rouen : le pont de Normandie, le pont de Tancarville et le pont de Brotonne.Ils ne remontent pas plus haut que le pont Guillaume-le-Conquérant, ce dernier étant trop bas pour permettre aux bateaux de passer, seuls les sous-marins remontent plus en amont. Les navires accostent alors rive droite et rive gauche sur plusieurs kilomètres. Pour permettre aux navires de retrouver ce lieu en 2008, le sixième pont de Rouen, le pont Gustave-Flaubert (en aval du pont Guillaume-le-Conquérant) a été spécialement conçu pour laisser passer les navires.</p> 
                 <h3>La manifestation</h3>  
                 <p> Durant les dix jours, les quartiers de Rouen sont aux couleurs des différentes nationalités des 
                  bateaux invités. Près de 8 000 marins sont présents dans les rues rouennaises. Des concerts ont lieu tous les jours sur les quais et chaque soir un feu d'artifice est tiré. Le feu d'artifice de clôture est particulièrement soigné. La patrouille de France est également présente pour l'occasion. D'autres animations sont également au programme.</p>
                 <h3>La parade de la Seine</h3>
                 <p>Au terme de ces dix jours, les navires descendent la Seine les uns à la suite des autres. Cette « parade de la Seine » s'achève au large de Honfleur et du Havre et les navires retrouvent alors la mer.</p>     
            </div>
             <!--Contacts-->
             <div class="col-sm-4">
                 <div class="card border-info mb-4" style=" margin-left: 0px;">
                     <div class="card-header">Contacts</div>
                         <div class="card-body text-info">
                             <h5 class="card-title">L'équipe Armada est disponible via l'adresse et le numéro de telephone ci dessous :</h5>
                               <p class="card-text">Association "Armada de la liberté"<br>
                                    Hangar 23 - 23, Boulevard Emile Duchemin - 76000 ROUEN<br>
                                    Tél. : 02.35.89.20.03</p>
                         </div>
                 </div>  
             </div>
         </div>
    </div>

  </body>

  <?php include("footer.inc.php"); ?>
</html>