<?php

    if(!empty($_FILES['image']) && $_FILES['image']['error'] != 4){
        //1-Verifie si le fichier exixste 
        $dossier2 = "Uploads/";
        $chemain = $dossier2. basename($_FILES["image"]["name"]);
        if (file_exists($chemain)) {
            $errImage = " * Le fichier existe deja!";
            $success_i = false;
        }
    
        //2- Validation de l'extension
        $extension = array('png','jpg','jpeg'); //Tableau d'extensions à vérifier
        $extension_fichier = strtolower(substr(strrchr($_FILES["image"]["name"],'.'),1)); //Releve l'extension du fichier à l'aide de la variable globale en ne retenant que la partie qui suit immediatement le point
        if(!in_array($extension_fichier,$extension)){
            $errImage = " * Vous devez transferer un fichier de type PNG, JPG ou JPEG!";
            $success_i = false;
        }
    
        //3-Validation de la taille
        $tailles_max = 2097125;
        $tailles = filesize($_FILES["image"]["tmp_name"]);
        if($tailles > $tailles_max){
        $errImage = " * La taille de l'image depasse 2Mo. Veuillez recommencer!";
        $success_i = false;
        }
    
        $fichiers = basename($_FILES["image"]["name"]); //Recupération du nom du fichier
    
    }else{
        $errImage = " * Veuillez choisir une image!";
    }

?>